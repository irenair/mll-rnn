from rnn import RNN
import pandas as pd
import numpy

def main():
    input_matrix= [[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,1,0]]

    ## preprocessing
    inputm = pd.read_csv('train_IBM.csv')
    inputm.dropna(inplace=True)
    # inputm = inputm['Close'].values.tolist()
    # print(inputm)

    lines = round(0.0019 * len(inputm))
    train = inputm.loc[:int(lines), :]
    test = inputm.loc[int(lines):, :]
    test = test.reindex()

    X_train = []
    y_train = []
    X_test = []
    y_test = []
    y_test_date = []
    seq_len = 3
    for i in range(seq_len, len(train)): 
        X_train.append(train.iloc[i-seq_len:i, 4]) 
        y_train.append(train.iloc[i, 4])
    for i in range(seq_len, len(test)): 
        X_test.append(test.iloc[i-seq_len:i, 4]) 
        y_test.append(test.iloc[i, 4])
        y_test_date.append(test.iloc[i, 0])

    print(len(X_train))
    X_train = [a.tolist() for a in X_train]
    print('Input matrix:\n',X_train)
    

    ## classification
    hidden_size= 3 #h
    output_size= 3
    timestep = 3 # sequence length
    return_sequences = False
    rnn = RNN(X_train,hidden_size,output_size)
    # rnn = RNN(input_matrix,hidden_size,output_size)
    output = rnn.forward_prop(timestep,return_sequences)
    # print(output[3])
    pass
main()