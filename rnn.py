import numpy as np
from scipy.special import softmax
class RNN():
    def __init__(self,input_matrix,hidden_size,output_size):
        self.input_matrix = input_matrix
        self.hidden_size = hidden_size
        self.output_size = output_size
        # self.bias_xh = np.asarray([[0.100],[0.100],[0.100]])
        # self.bias_hy = np.asarray([[0.100],[0.100],[0.100],[0.100]])
        # self.u = [[0.100,0.150,0.200,0.300],[0.150,0.200,0.300,0.100],[0.200,0.300,0.100,0.150]]
        # self.w = [[0.500,0.500,0.500],[0.500,0.500,0.500],[0.500,0.500,0.500]]
        # self.v = [[0.100,0.200,0.300],[0.200,0.300,0.100],[0.300,0.100,0.200],[0.100,0.100,0.100]]
        self.bias_xh = np.random.rand(self.hidden_size,1)
        self.bias_hy = np.random.rand(self.output_size,1)

        self.v = np.asarray(self.create_v(self.output_size,self.hidden_size))
        self.w = np.asarray(self.create_w(self.hidden_size))

    def create_u(self,hidden_size,input_dim):
        u = np.random.rand(hidden_size,input_dim)
        return u
        pass
    def create_v(self,output_size,hidden_size):
        v = np.random.rand(output_size,hidden_size)
        return v
        pass
    def create_w(self,hidden_size):
        # w =  np.random.rand(hidden_size,hidden_size)
        w = np.full((hidden_size,hidden_size),1)
        # print(w)
        return w
        pass
    def ht(self,u,w,h_prev,bias_xh,i):
        input_matrix = np.reshape(np.asarray(self.input_matrix[i]),(len(self.input_matrix[i]),1))
        uxt = np.dot(u,input_matrix)
        wh_prev = np.dot(w,h_prev)
        net = wh_prev + bias_xh
        res = np.tanh(uxt + (net))
        rounded_result = np.round(res,3)
        return rounded_result
        pass
    def yt(self,v,ht,bias_hy):
        vht = np.dot(v,ht)
        net = vht+bias_hy 
        res = softmax(net)
        rounded_result = np.round(res,3)
        return rounded_result
        pass
    
    def calculate_timestep(self,h_prev,input_matrix,i):
        # print hidden state
        
        self.u = np.asarray(self.create_u(self.hidden_size,len(input_matrix[i])))

        # print('u shape',np.asarray(self.u).shape)
        # print('v shape',np.asarray(self.v).shape)
        # print('w shape',np.asarray(self.w).shape)

        ht = self.ht(self.u,self.w,h_prev,self.bias_xh,i)
        print('ht shape timestep-{}: {}'.format(i,np.asarray(ht).shape))
        print(ht)

        # print output
        yt = self.yt(self.v,ht,self.bias_hy)
        print('yt shape timestep-{}: {}'.format(i,np.asarray(yt).shape))
        print(yt)
        return ht,yt
        pass
    
    def forward_prop(self,timestep,return_sequences):
        hidden_state = []
        output_state = []
        for i in range(0,timestep):
            if (i == 0):
                h_prev = np.zeros(np.asarray(self.bias_xh).shape)
            else:
                h_prev = hidden_state[i-1]
            
            hidden,output = self.calculate_timestep(h_prev,self.input_matrix,i)
            rounded_hidden = np.round(hidden,3)
            hidden_state.append(rounded_hidden)

            rounded_output = np.round(output,3)            
            output_state.append(rounded_output)
        
        if (return_sequences):
            return output_state
        else:
            return output_state[len(output_state)-1]
    pass